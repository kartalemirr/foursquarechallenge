//
//  Extensions.swift
//  fourSquareChallenge
//
//  Created by Emir Kartal on 2.06.2019.
//  Copyright © 2019 Emir Kartal. All rights reserved.
//

import Foundation

extension Dictionary {
    func dictToJSON() -> AnyObject {
        let jsonData = try! JSONSerialization.data(withJSONObject: self, options: .prettyPrinted)
        let decoded = try! JSONSerialization.jsonObject(with: jsonData, options: [])
        return decoded as AnyObject
    }
}

extension String {
    
    func toAppropriateDate() -> String {
        let dateFormatter = DateFormatter()
        if self.count > 25 {
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        } else {
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        }
        
        dateFormatter.locale = Locale.current
        if let date = dateFormatter.date(from: self) {
            
            let myDateFormatter = DateFormatter()
            myDateFormatter.dateFormat = "dd/MM/yyyy HH:mm"
            //myDateFormatter.locale = Locale.current
            
            return myDateFormatter.string(from: date)
        }
        
        return ""
    }
    
    func toOnlyTime() -> String {
        let dateFormatter = DateFormatter()
        if self.count > 25 {
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        } else {
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        }
        
        dateFormatter.locale = Locale.current
        if let date = dateFormatter.date(from: self) {
            
            let myDateFormatter = DateFormatter()
            myDateFormatter.dateFormat = "HH:mm"
            //myDateFormatter.locale = Locale.current
            
            return myDateFormatter.string(from: date)
        }
        
        return ""
    }
    
    func toOnlyDate() -> String {
        let dateFormatter = DateFormatter()
        if self.count > 25 {
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        } else {
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        }
        
        dateFormatter.locale = Locale.current
        if let date = dateFormatter.date(from: self) {
            
            let myDateFormatter = DateFormatter()
            myDateFormatter.dateFormat = "yyyy/MM/dd"
            //myDateFormatter.locale = Locale.current
            
            return myDateFormatter.string(from: date)
        }
        
        return ""
    }
}
