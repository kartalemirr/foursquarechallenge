//
//  DetailTipCell.swift
//  fourSquareChallenge
//
//  Created by Emir Kartal on 3.06.2019.
//  Copyright © 2019 Emir Kartal. All rights reserved.
//

import UIKit

class DetailTipCell: UITableViewCell {

    @IBOutlet weak var tipTextLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
