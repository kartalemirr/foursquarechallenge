//
//  PlaceCell.swift
//  fourSquareChallenge
//
//  Created by Emir Kartal on 2.06.2019.
//  Copyright © 2019 Emir Kartal. All rights reserved.
//

import UIKit

class PlaceCell: UITableViewCell {

    @IBOutlet weak var placeNameLbl: UILabel!
    @IBOutlet weak var placeAdressLabel: UILabel!
    @IBOutlet weak var placeCountryLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func populateCellData(place: VenueModel) {
        placeNameLbl.text = place.name
        placeAdressLabel.text = place.location?.formattedAddress?[0]
        placeCountryLabel.text = place.location?.country
    }
}
