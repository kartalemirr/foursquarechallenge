//
//  ViewController.swift
//  fourSquareChallenge
//
//  Created by Emir Kartal on 2.06.2019.
//  Copyright © 2019 Emir Kartal. All rights reserved.
//

import UIKit
import CoreLocation

class SearchVC: BaseViewController, CLLocationManagerDelegate {

    @IBOutlet weak var categoryTextField: UITextField!
    @IBOutlet weak var locationTextField: UITextField!
    
    var locationManager: CLLocationManager!
    var lat: String?
    var lng: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getLocation()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        locationManager.stopUpdatingLocation()
    }
    
    @IBAction func searchBtnTapped(_ sender: Any) {
        
        if checkLength(text: categoryTextField.text ?? "") {
            if locationTextField.text == "" || locationTextField.text == nil {
                
                GetVenues(lat: lat!, lng: lng!, location: nil, query: categoryTextField.text!)
            } else {
                GetVenues(lat: nil, lng: nil, location: locationTextField.text, query: categoryTextField.text!)
            }
            
        } else {
            let alert = UIAlertController(title: "Warning", message: "You need to write category", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        
        
    }
    
    //MARK: Location of user
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let location = locations.last! as CLLocation
        
        lat = "\(location.coordinate.latitude)"
        lng = "\(location.coordinate.longitude)"
        
    }
    
    func getLocation() {
        if CLLocationManager.locationServicesEnabled() {
            locationManager = CLLocationManager()
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestWhenInUseAuthorization()
            locationManager.startUpdatingLocation()
        }
    }

    //MARK: List of venue according to location
    
    func GetVenues(lat: String?, lng: String?, location: String?, query: String) {
        
        Network.getVenues(viewController: self, date: self.getDate(), lat: lat, lng: lng, location: location, query: query) { [weak self] (rsp) in
            
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            var placesViewController = PlacesVC()
            placesViewController = storyBoard.instantiateViewController(withIdentifier: "PlacesVC") as! PlacesVC
            placesViewController.listOfPlaces = rsp?.response?.venues
            self?.navigationController?.pushViewController(placesViewController, animated: true)
        }
        
    }
    
    //MARK: - Check Search Conditions
    
    // Prevent searching when user write two letter or less
    func checkLength(text: String) -> Bool {
        let length = (text.count)
        
        if length > 2 {
            return true
        } else {
            return false
        }
        
        
    }
    
    
//    func getDate() -> String {
//
//        let date = Date()
//        let formatter = DateFormatter()
//        formatter.dateFormat = "yyyyMMdd"
//
//        let result = formatter.string(from: date)
//
//        return result
//    }

}

