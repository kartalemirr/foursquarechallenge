//
//  PlacesVC.swift
//  fourSquareChallenge
//
//  Created by Emir Kartal on 2.06.2019.
//  Copyright © 2019 Emir Kartal. All rights reserved.
//

import UIKit

class PlacesVC: BaseViewController {

    var listOfPlaces: [VenueModel]?
    private var placeCellIdentifier = "PlaceCell"
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setTableView()
        
    }
    
    func setTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: placeCellIdentifier, bundle: nil), forCellReuseIdentifier: placeCellIdentifier)
    }
    
}

//MARK: Tableview delegate and datasource methods

extension PlacesVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listOfPlaces?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: placeCellIdentifier) as! PlaceCell
        if let venue = listOfPlaces?[indexPath.row] {
            cell.populateCellData(place: venue)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let venue = listOfPlaces?[indexPath.row] {
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let detailVC = storyBoard.instantiateViewController(withIdentifier: "PlaceDetailVC") as! PlaceDetailVC
            detailVC.venue = venue
            detailVC.modalPresentationStyle = .overCurrentContext
            self.present(detailVC, animated: true, completion: nil)
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
}
