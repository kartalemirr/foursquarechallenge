//
//  PlaceDetailVC.swift
//  fourSquareChallenge
//
//  Created by Emir Kartal on 2.06.2019.
//  Copyright © 2019 Emir Kartal. All rights reserved.
//

import UIKit
import MapKit
import Kingfisher

class PlaceDetailVC: BaseViewController {

    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var venueImageView: UIImageView!
    @IBOutlet weak var containerView: UIView!
    
    var venue: VenueModel?
    private let detailTipCellIdentifier = "DetailTipCell"
    var tipArray: [TipItemModel] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUI()
        setTapRecognizer()
        setTableView()
        
        if let lat = venue?.location?.lat, let lng = venue?.location?.lng {
            getVenueViewOnMap(lat: lat, lng: lng, name: venue?.name ?? "")
        }
        
        getVenueDetail(venueId: (venue?.id)!)
        
    }
    
    func setUI() {
        containerView.layer.cornerRadius = 15
        containerView.layer.masksToBounds = true
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        self.dismiss(animated: false, completion: nil)
    }
    
    func setTableView(){
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: detailTipCellIdentifier , bundle: nil), forCellReuseIdentifier: detailTipCellIdentifier)
    }
    
    func setTapRecognizer() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap(_:)))
        tap.delegate = self
        tap.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tap)
    }
    
    //MARK: Venue Detail
    
    func getVenueDetail(venueId: String) {
        Network.getVenueDetail(viewController: self, venueID: venueId, date: self.getDate()) { [weak self] (rsp) in
            self?.tipArray = rsp?.response?.venue?.tips?.groups?[0].items ?? []
            if let prefix = rsp?.response?.venue?.photos?.groups?[1].items?[0].prefix,
                let suffix = rsp?.response?.venue?.photos?.groups?[1].items?[0].suffix {
                
                let stringURL = "\(prefix)512x512\(suffix)"
                if let url = URL(string: stringURL) {
                    self?.venueImageView.kf.setImage(with: url)
                }
                
            }
            
            self?.tableView.reloadData()
        }
    }
    
    //MARK: Location of venue
    
    func getVenueViewOnMap(lat: Double, lng: Double, name: String) {
        
        
        let location = CLLocationCoordinate2D(latitude: lat,
                                              longitude: lng)
        
        let span = MKCoordinateSpan(latitudeDelta: 0.05, longitudeDelta: 0.05)
        let region = MKCoordinateRegion(center: location, span: span)
        mapView.setRegion(region, animated: true)
        
        let annotation = MKPointAnnotation()
        annotation.coordinate = location
        annotation.title = name
        mapView.addAnnotation(annotation)
    }

}

// MARK Tableview delegate and datasource methods

extension PlaceDetailVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tipArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //TODO: Check tips count
        let cell = tableView.dequeueReusableCell(withIdentifier: detailTipCellIdentifier) as! DetailTipCell
        let tip = tipArray[indexPath.row]
        cell.tipTextLabel.text = tip.text
        return cell
    }
    
    
}

//MARK: Gesture Recognizer Delegate Extension

// Identify tapping out of the containerView bounds in order to dismiss detail page

extension PlaceDetailVC: UIGestureRecognizerDelegate {
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        return (touch.view === self.view)
    }
}
