//
//  Network.swift
//  fourSquareChallenge
//
//  Created by Emir Kartal on 2.06.2019.
//  Copyright © 2019 Emir Kartal. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper
import NVActivityIndicatorView

class Network {
    //https://api.foursquare.com/v2/venues/search?v=20191029&venuePhotos=1&ll=-23.5666151,-46.6463977&limit=10&client_id=B1A5KAA5NWNAR1POR2552TJ01KRUX4GZQGUXVSOWF1ZZCSMU&client_secret=VMSKQYHRVASX5UP1AFCH0TR0OFLFT1J2HNKUOE3DIABXA5UD
    
    //GET https://api.foursquare.com/v2/venues/VENUE_ID/tips
    

    static let apiEndPoint = "https://api.foursquare.com/v2/"
    
    private static let searchVenues = "venues/search"
    private static let venueDetail = "venues/"
    private static let venueTips = "venues/"
 
    private static func request<T: BaseResponseModel> (
        method: HTTPMethod,
        parameters: Parameters?,
        url: String,
        viewController: BaseViewController?,
        encoding: ParameterEncoding,
        responseModel: T.Type,
        showHud: Bool,
        showErrorAlerts: Bool,
        completion: @escaping (T?) -> Void) {
        
        //        HUD.hide()
        viewController?.stopAnimating()

        if showHud {
            viewController?.startAnimating(CGSize(width: 60, height:60), message: "", type: NVActivityIndicatorType.ballClipRotateMultiple)
            //            HUD.show(.progress)
        }
        
        print("Request: \(String(describing: parameters?.dictToJSON() ?? "Request is Empty" as AnyObject))")
        print("Request URL: \(url)")
        Alamofire.request(url, method: method, parameters: parameters ?? nil, encoding: encoding, headers: nil).responseObject{ ( response : DataResponse<T>) in
            
            if showHud {
                viewController?.stopAnimating()

            }
            
            print("Success: \(response.result.isSuccess)")
            print("Response: \(String(describing: response.result.value?.toJSON().dictToJSON() ?? "" as AnyObject))")
            
            var statusCode = response.response?.statusCode
            if let error = response.result.error as? AFError {
                statusCode = error._code // statusCode private
                switch error {
                case .invalidURL(let url):
                    print("Invalid URL: \(url) - \(error.localizedDescription)")
                case .parameterEncodingFailed(let reason):
                    print("Parameter encoding failed: \(error.localizedDescription)")
                    print("Failure Reason: \(reason)")
                case .multipartEncodingFailed(let reason):
                    print("Multipart encoding failed: \(error.localizedDescription)")
                    print("Failure Reason: \(reason)")
                case .responseValidationFailed(let reason):
                    print("Response validation failed: \(error.localizedDescription)")
                    print("Failure Reason: \(reason)")
                    
                    switch reason {
                    case .dataFileNil, .dataFileReadFailed:
                        print("Downloaded file could not be read")
                    case .missingContentType(let acceptableContentTypes):
                        print("Content Type Missing: \(acceptableContentTypes)")
                    case .unacceptableContentType(let acceptableContentTypes, let responseContentType):
                        print("Response content type: \(responseContentType) was unacceptable: \(acceptableContentTypes)")
                    case .unacceptableStatusCode(let code):
                        print("Response status code was unacceptable: \(code)")
                        statusCode = code
                    }
                case .responseSerializationFailed(let reason):
                    print("Response serialization failed: \(error.localizedDescription)")
                    print("Failure Reason: \(reason)")
                    // statusCode = 3840 ???? maybe..
                }
                
                print("Underlying error: \(String(describing: error.underlyingError))")
            } else if let error = response.result.error as? URLError {
                print("URLError occurred: \(error)")
            } else {
                print("Unknown error: \(String(describing: response.result.error))")
            }
            
            print(statusCode ?? "")
            
            if viewController != nil {
                
                guard let requestResponseValue = response.result.value else {
                    return
                }
                
                completion(requestResponseValue)
            }
            
        }
    }
    
    static func getVenues(viewController: BaseViewController,date: String, lat: String?,lng: String?,location: String?,query: String,completion: @escaping (SearchVenueModel?) -> Void) {
        
        var queryParams = ""
        
        if location != nil {
            queryParams = "?v=\(date)&venuePhotos=1&near=\(String(describing: location!))&limit=30&query=\(query)&client_id=\(Constants.clientId)&client_secret=\(Constants.clientSecret)"
        } else {
            queryParams = "?v=\(date)&venuePhotos=1&ll=\(String(describing: lat!)),\(String(describing: lng!))&limit=30&query=\(query)&client_id=\(Constants.clientId)&client_secret=\(Constants.clientSecret)"
        }
        
        let url = "\(apiEndPoint)\(searchVenues)\(queryParams)"
        
        self.request(method: .get, parameters: nil , url: url, viewController: viewController, encoding: JSONEncoding.default, responseModel: SearchVenueModel.self, showHud: true, showErrorAlerts: true) { response in
            completion(response)
        }
    }
    
    static func getVenueDetail(viewController: BaseViewController,venueID: String,date: String,completion: @escaping (VenueDetailResponseModel?) -> Void) {
        let queryParams = "?v=\(date)&client_id=\(Constants.clientId)&client_secret=\(Constants.clientSecret)"
        let url = "\(apiEndPoint)\(venueDetail)\(venueID)\(queryParams)"
        
        self.request(method: .get, parameters: nil , url: url, viewController: viewController, encoding: JSONEncoding.default, responseModel: VenueDetailResponseModel.self, showHud: true, showErrorAlerts: true) { response in
            completion(response)
        }
    }
    
    static func getVenueTips (viewController: BaseViewController,venueID: String,date: String,completion: @escaping (VenueDetailResponseModel?) -> Void) {
        let queryParams = "?v=20190101&client_id=\(Constants.clientId)&client_secret=\(Constants.clientSecret)"
        let url = "\(apiEndPoint)\(venueTips)\(venueID)/tips\(queryParams)"
        
        self.request(method: .get, parameters: nil , url: url, viewController: viewController, encoding: JSONEncoding.default, responseModel: VenueDetailResponseModel.self, showHud: true, showErrorAlerts: true) { response in
            completion(response)
        }
    }
    
    
}
