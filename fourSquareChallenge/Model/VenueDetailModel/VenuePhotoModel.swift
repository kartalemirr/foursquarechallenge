//
//  VenuePhotoModel.swift
//  fourSquareChallenge
//
//  Created by Emir Kartal on 14.06.2019.
//  Copyright © 2019 Emir Kartal. All rights reserved.
//

import Foundation
import ObjectMapper

class VenuePhotoModel: Mappable {
    
    var count: Int?
    var groups: [PhotoGroupModel]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        count <- map["count"]
        groups <- map["groups"]
    }
    
}
class PhotoGroupModel: Mappable {
    
    var items: [PhotoItemModel]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        items <- map["items"]
        
    }
}

class PhotoItemModel: Mappable {
    
    var id : String?
    var prefix: String?
    var suffix: String?
    var width: Int?
    var height: Int?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        id <- map["id"]
        prefix <- map["prefix"]
        suffix <- map["suffix"]
        width <- map["width"]
        height <- map["height"]
        
    }
}
