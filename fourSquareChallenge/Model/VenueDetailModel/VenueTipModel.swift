//
//  VenueTipModel.swift
//  fourSquareChallenge
//
//  Created by Emir Kartal on 14.06.2019.
//  Copyright © 2019 Emir Kartal. All rights reserved.
//

import Foundation
import ObjectMapper

class VenueTipModel: Mappable {
    
    var count: Int?
    var groups: [TipGroupModel]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        count <- map["count"]
        groups <- map["groups"]
    }
    
}

class TipGroupModel: Mappable {
    
    var items: [TipItemModel]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        items <- map["items"]
        
    }
}

class TipItemModel: Mappable {
    
    var id: String?
    var text: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        text <- map["text"]
        
        
    }
}
