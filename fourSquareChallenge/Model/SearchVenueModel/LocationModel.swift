//
//  LocationModel.swift
//  fourSquareChallenge
//
//  Created by Emir Kartal on 2.06.2019.
//  Copyright © 2019 Emir Kartal. All rights reserved.
//

import Foundation
import ObjectMapper

class LocationModel: Mappable {
    
    var formattedAddress: [String]?
    var lat: Double?
    var lng: Double?
    var country: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        formattedAddress <- map["formattedAddress"]
        lat <- map["lat"]
        lng <- map["lng"]
        country <- map["country"]
        
    }

}
