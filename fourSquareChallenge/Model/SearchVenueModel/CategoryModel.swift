//
//  CategoryModel.swift
//  fourSquareChallenge
//
//  Created by Emir Kartal on 2.06.2019.
//  Copyright © 2019 Emir Kartal. All rights reserved.
//

import Foundation
import ObjectMapper


class CategoryModel: Mappable {
    
    var id: String?
    var name: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        
    }
    
}
