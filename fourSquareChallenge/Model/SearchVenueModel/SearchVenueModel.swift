//
//  SearchVenueModel.swift
//  fourSquareChallenge
//
//  Created by Emir Kartal on 2.06.2019.
//  Copyright © 2019 Emir Kartal. All rights reserved.
//

import Foundation
import ObjectMapper

class SearchVenueModel: BaseResponseModel {
    
    var response : VenuesModel?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        response <- map["response"]
        
    }
}

class VenuesModel: Mappable {
    
    var venues: [VenueModel]?
    var confident: Bool?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        venues <- map["venues"]
        confident <- map ["confident"]
    }
    
}


