//
//  VenueModel.swift
//  fourSquareChallenge
//
//  Created by Emir Kartal on 2.06.2019.
//  Copyright © 2019 Emir Kartal. All rights reserved.
//

import Foundation
import ObjectMapper

class VenueModel: Mappable {
    
    var id: String?
    var name: String?
    var location: LocationModel?
    var categories: [CategoryModel]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        location <- map["location"]
        categories <- map["categories"]
    }
    
}
/*
 {
 "id": "5642aef9498e51025cf4a7a5",
 "name": "Mr. Purple",
 "location": {
 "address": "180 Orchard St",
 "crossStreet": "btwn Houston & Stanton St",
 "lat": 40.72173744277209,
 "lng": -73.98800687282996,
 "labeledLatLngs": [
 {
 "label": "display",
 "lat": 40.72173744277209,
 "lng": -73.98800687282996
 }
 ],
 "distance": 8,
 "postalCode": "10002",
 "cc": "US",
 "city": "New York",
 "state": "NY",
 "country": "United States",
 "formattedAddress": [
 "180 Orchard St (btwn Houston & Stanton St)",
 "New York, NY 10002",
 "United States"
 ]
 },
 "categories": [
 {
 "id": "4bf58dd8d48988d1d5941735",
 "name": "Hotel Bar",
 "pluralName": "Hotel Bars",
 "shortName": "Hotel Bar",
 "icon": {
 "prefix": "https://ss3.4sqi.net/img/categories_v2/travel/hotel_bar_",
 "suffix": ".png"
 },
 "primary": true
 }
 ],
 "venuePage": {
 "id": "150747252"
 }
 }
 ]
 
 
 */
