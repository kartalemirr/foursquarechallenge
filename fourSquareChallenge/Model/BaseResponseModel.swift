//
//  BaseResponseModel.swift
//  fourSquareChallenge
//
//  Created by Emir Kartal on 2.06.2019.
//  Copyright © 2019 Emir Kartal. All rights reserved.
//
import UIKit
import ObjectMapper

class BaseResponseModel: Mappable {
    
    var meta: MetaModel?
    
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        meta <- map["meta"]
        
    }
}

class MetaModel: Mappable {
    
    var code: Int?
    var requestId: String?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        code <- map["code"]
        requestId <- map["requestId"]
    }
}
